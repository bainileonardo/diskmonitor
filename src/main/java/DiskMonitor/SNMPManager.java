package DiskMonitor;

/*
 * Para habilitar snmp en equipos Windows https://www.youtube.com/watch?v=Ic-OjYb9Zhk
 * 
 * */
import java.io.IOException;

import org.snmp4j.CommunityTarget;
import org.snmp4j.PDU;
import org.snmp4j.Snmp;
import org.snmp4j.Target;
import org.snmp4j.TransportMapping;
import org.snmp4j.event.ResponseEvent;
import org.snmp4j.mp.SnmpConstants;
import org.snmp4j.smi.Address;
import org.snmp4j.smi.GenericAddress;
import org.snmp4j.smi.OID;
import org.snmp4j.smi.OctetString;
import org.snmp4j.smi.VariableBinding;
import org.snmp4j.transport.DefaultUdpTransportMapping;

public class SNMPManager {

	Snmp snmp = null;
	String address = null;

	public SNMPManager(String add)
	{
		address = add;
	}

	public static void main(String[] args) throws IOException {
		/**
		 * Port 161 is used for Read and Other operations
		 * Port 162 is used for the trap generation
		 */
		/* para test
		args=new String[2];
		args[0]="127.0.0.1";
		args[1]="2";
		*/
		
		
		String ip=null;
		String opcion=null;
		
		System.out.println("Argumentos ingresados:"+args.length);
		if(args.length==0) {
			System.out.println("Ingrese argumentos \n"
					+ "puede ser IP y OID: \n"
					+ "Ejemplo 1 (porcentaje de uso en disco): java -jar DiskQuery.jar 192.168.1.20 1\n"
					+ "Ejemplo 2: (porcentaje libre en disco): java -jar DiskQuery.jar 192.168.1.20 2\n"
					+ "Ejemplo 3: (Info resumida (salida por consola)): java -jar DiskInfo.jar 192.168.1.20 3\n"
					); 
			System.exit(-1);
		}else if(args.length!=2 && args.length!=0) {
			System.out.println("Debe ingresar IP + OPCION, ej 192.168.1.20 1");
			System.exit(-2);
		}else if (args.length==2) {	

			ip=args[0];
			opcion=args[1];


			OID memoria=new OID(".1.3.6.1.2.1.25.2.2.0");
			OID discoTotal=new OID(".1.3.6.1.2.1.25.2.3.1.5.1");
			OID espacioUsado=new OID(".1.3.6.1.2.1.25.2.3.1.6.1");
			OID discAllocationUnit=new OID(".1.3.6.1.2.1.25.2.3.1.4.1");


			SNMPManager client = new SNMPManager("udp:"+ip+"/161");
			client.start();

			String s_discoTotal = client.getAsString(new OID(discoTotal));
			String s_memoria = client.getAsString(new OID(memoria));
			String s_allocated=client.getAsString(discAllocationUnit);
			String s_discoUsado=client.getAsString(espacioUsado);
			
			int aux=0;// es para que sirva de retorno en el system exit
			
			switch(opcion) {
			case "1":aux=(int) Math.floor( porcentajeDiscoUsado(s_discoTotal, s_discoUsado, s_allocated));System.out.println("Porcentaje de disco usado es: %"+aux);System.exit(aux);break;
			case "2":aux=(int) Math.floor(porcentajeEspacioLibre(s_discoTotal, s_discoUsado, s_allocated));System.out.println("Porcentaje de disco libre es: %"+aux);System.exit(aux);break;
			case "3":infoGral(s_discoTotal, s_discoUsado, s_memoria, s_allocated);break;
			default: System.out.println("Opci�n no encontrada, seleccione una v�lida");
			}
			
			

		}
	}
	
	private static void infoGral(String s_discoTotal,String s_discoUsado,String s_memoria,String s_allocated) {
		System.out.println("Tu pc tiene "+ Float.parseFloat(s_memoria)/1024/1024+" Memoria instalada");
		System.out.println("Su pc tiene "+TamanioDisco(s_discoTotal, s_allocated)+" GB en total");
		System.out.println("Espacio usado es "+TamanioDisco(s_discoUsado, s_allocated));
		System.out.println("Espacio disponible es "+porcentajeEspacioLibre(s_discoTotal, s_discoUsado, s_allocated));
		System.out.println("Porcentaje de uso es "+porcentajeDiscoUsado(s_discoTotal, s_discoUsado, s_allocated));
		System.out.println("Porcentaje restante de uso es "+(100-porcentajeDiscoUsado(s_discoTotal, s_discoUsado, s_allocated)));
		}
	
	private static float porcentajeDiscoUsado(String s_discoTotal,String s_discoUsado,String s_allocated) {
		float discoTotal=TamanioDisco(s_discoTotal, s_allocated);
		float discoUsado=TamanioDisco(s_discoUsado, s_allocated);
		return (discoUsado*100)/discoTotal;

	}
	private static float porcentajeEspacioLibre(String s_discoTotal,String s_discoUsado,String s_allocated) {
		float discoTotal=TamanioDisco(s_discoTotal, s_allocated);
		float discoUsado=TamanioDisco(s_discoUsado, s_allocated);
		float discoLibre=discoTotal-discoUsado;		
		return (discoLibre*100)/discoTotal;
	}

	private static float convertirFloat(String valor) {
		return Float.parseFloat(valor);

	}
	private static float TamanioDisco(String s_discoTotal,String allocated) {
		float divisor=(float) Math.pow(1024, 3);
		float producto=convertirFloat(s_discoTotal)*convertirFloat(allocated);
		return producto/divisor;

	}
	/**
	 * Start the Snmp session. If you forget the listen() method you will not
	 * get any answers because the communication is asynchronous
	 * and the listen() method listens for answers.
	 * @throws IOException
	 */
	private void start() throws IOException {
		TransportMapping<?> transport = new DefaultUdpTransportMapping();
		snmp = new Snmp(transport);
		// Do not forget this line!
		transport.listen();
	}

	/**
	 * Method which takes a single OID and returns the response from the agent as a String.
	 * @param oid
	 * @return
	 * @throws IOException
	 */
	public String getAsString(OID oid) throws IOException {
		ResponseEvent<?> event = get(new OID[] { oid });
		return event.getResponse().get(0).getVariable().toString();
	}

	/**
	 * This method is capable of handling multiple OIDs
	 * @param oids
	 * @return
	 * @throws IOException
	 */
	public ResponseEvent<?> get(OID oids[]) throws IOException {
		PDU pdu = new PDU();
		for (OID oid : oids) {
			pdu.add(new VariableBinding(oid));
		}
		pdu.setType(PDU.GET);
		ResponseEvent<?> event = snmp.send(pdu, getTarget(), null);
		if(event != null) {
			return event;
		}
		throw new RuntimeException("GET timed out");
	}

	/**
	 * This method returns a Target, which contains information about
	 * where the data should be fetched and how.
	 * @return
	 */
	private Target<Address> getTarget() {
		Address targetAddress = GenericAddress.parse(address);
		CommunityTarget<Address> target = new CommunityTarget<Address>();
		target.setCommunity(new OctetString("public"));
		target.setAddress(targetAddress);
		target.setRetries(2);
		target.setTimeout(1500);
		target.setVersion(SnmpConstants.version2c);
		return target;
	}

}